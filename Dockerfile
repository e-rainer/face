FROM    ros:melodic

RUN     apt  update
RUN     apt  upgrade -y
RUN     apt  install -y python3-pip curl vim mc less ros-melodic-rosserial ros-melodic-rosserial-arduino python3-empy
RUN     pip3 install platformio catkin_pkg pyyaml

WORKDIR /face_ws
RUN     mkdir src
RUN     . /opt/ros/melodic/setup.sh && catkin_make -DPYTHON_EXECUTABLE=/usr/bin/python3

ADD     face /face_ws/src/face
RUN     . /opt/ros/melodic/setup.sh && catkin_make
RUN     . /opt/ros/melodic/setup.sh && catkin_make install

ADD     face.launch /
ADD     arduino /arduino
RUN     . /opt/ros/melodic/setup.sh && . /face_ws/install/setup.sh && rosrun rosserial_arduino make_libraries.py /arduino/lib/

WORKDIR /arduino
RUN     pio run
