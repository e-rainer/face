#include <Arduino.h>
#include <ArduinoHardware.h>
#include <ros.h>
#include <Wire.h>
#include <Adafruit_PWMServoDriver.h>
#include <face/Servo_Array.h>
#include "main.h"
#include "face.h"

Adafruit_PWMServoDriver pwm = Adafruit_PWMServoDriver();

void face_init(void){
  pwm.begin();
  pwm.setOscillatorFrequency(27000000);
  pwm.setPWMFreq(SERVO_FREQ);
  delay(10);
}

void messageCb( const face::Servo_Array& msg){
    digitalWrite(13, HIGH-digitalRead(13));  //toggle led

    // NODDING
    if(msg.nodding < 0){
        pwm.setPWM(SERVO_NODDING, 0, NODDING_CENTER - ((NODDING_CENTER - NODDING_MIN   ) * (((-1.0)*msg.nodding) / 100.0)));
    } else {
        pwm.setPWM(SERVO_NODDING, 0, NODDING_CENTER + ((NODDING_MAX    - NODDING_CENTER) * ((( 1.0)*msg.nodding) / 100.0)));
    }


    // LEFT
    if(msg.nodding < 0){
        pwm.setPWM(SERVO_LEFT_EYE, 0, LEFT_EYE_CENTER - ((LEFT_EYE_CENTER - LEFT_EYE_MIN   ) * (((-1.0)*msg.left_eye_pos) / 100.0)));
    } else {
        pwm.setPWM(SERVO_LEFT_EYE, 0, LEFT_EYE_CENTER + ((LEFT_EYE_MAX    - LEFT_EYE_CENTER) * ((( 1.0)*msg.left_eye_pos) / 100.0)));
    }

    if(msg.nodding < 0){
        pwm.setPWM(SERVO_LEFT_LIT, 0, LEFT_LIT_CENTER - ((LEFT_LIT_CENTER - LEFT_LIT_MIN   ) * (((-1.0)*msg.left_eye_lit) / 100.0)));
    } else {
        pwm.setPWM(SERVO_LEFT_LIT, 0, LEFT_LIT_CENTER + ((LEFT_LIT_MAX    - LEFT_LIT_CENTER) * ((( 1.0)*msg.left_eye_lit) / 100.0)));
    }

    if(msg.nodding < 0){
        pwm.setPWM(SERVO_LEFT_BROW, 0, LEFT_BROW_CENTER - ((LEFT_BROW_CENTER - LEFT_BROW_MIN   ) * (((-1.0)*msg.left_eye_eyebrow) / 100.0)));
    } else {
        pwm.setPWM(SERVO_LEFT_BROW, 0, LEFT_BROW_CENTER + ((LEFT_BROW_MAX    - LEFT_BROW_CENTER) * ((( 1.0)*msg.left_eye_eyebrow) / 100.0)));
    }


    // RIGHT
    if(msg.nodding < 0){
        pwm.setPWM(SERVO_RIGHT_EYE, 0, RIGHT_EYE_CENTER - ((RIGHT_EYE_CENTER - RIGHT_EYE_MIN   ) * (((-1.0)*msg.right_eye_pos) / 100.0)));
    } else {
        pwm.setPWM(SERVO_RIGHT_EYE, 0, RIGHT_EYE_CENTER + ((RIGHT_EYE_MAX    - RIGHT_EYE_CENTER) * ((( 1.0)*msg.right_eye_pos) / 100.0)));
    }

    if(msg.nodding < 0){
        pwm.setPWM(SERVO_RIGHT_LIT, 0, RIGHT_LIT_CENTER - ((RIGHT_LIT_CENTER - RIGHT_LIT_MIN   ) * (((-1.0)*msg.right_eye_lit) / 100.0)));
    } else {
        pwm.setPWM(SERVO_RIGHT_LIT, 0, RIGHT_LIT_CENTER + ((RIGHT_LIT_MAX    - RIGHT_LIT_CENTER) * ((( 1.0)*msg.right_eye_lit) / 100.0)));
    }

    if(msg.nodding < 0){
        pwm.setPWM(SERVO_RIGHT_BROW, 0, RIGHT_BROW_CENTER - ((RIGHT_BROW_CENTER - RIGHT_BROW_MIN   ) * (((-1.0)*msg.right_eye_eyebrow) / 100.0)));
    } else {
        pwm.setPWM(SERVO_RIGHT_BROW, 0, RIGHT_BROW_CENTER + ((RIGHT_BROW_MAX    - RIGHT_BROW_CENTER) * ((( 1.0)*msg.right_eye_eyebrow) / 100.0)));
    }

    pwm.writeMicroseconds(LED_LEFT_RED,    4095 * (msg.left_eye_red   / 100.0) );
    pwm.writeMicroseconds(LED_LEFT_GREEN,  4095 * (msg.left_eye_green / 100.0) );
    pwm.writeMicroseconds(LED_LEFT_BLUE,   4095 * (msg.left_eye_blue  / 100.0) );

    pwm.writeMicroseconds(LED_RIGHT_RED,   4095 * (msg.right_eye_red   / 100.0) );
    pwm.writeMicroseconds(LED_RIGHT_GREEN, 4095 * (msg.right_eye_green / 100.0) );
    pwm.writeMicroseconds(LED_RIGHT_BLUE,  4095 * (msg.right_eye_blue  / 100.0) );
}
