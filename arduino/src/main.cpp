#include <Arduino.h>
#include <ArduinoHardware.h>
#include <ros.h>
#include <face/Servo_Array.h>
#include "main.h"
#include "face.h"

ros::NodeHandle nh;
ros::Subscriber<face::Servo_Array> sub("erainer_face", &messageCb );

void setup(){
    pinMode(13, OUTPUT);
    face_init();
    nh.initNode();
    nh.subscribe(sub);
}

void loop(){
    nh.spinOnce();
    delay(1);
}
