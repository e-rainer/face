#ifndef  __ERAINER_FACE_H
#define  __ERAINER_FACE_H

#define SERVO_NODDING     11 // done

#define SERVO_LEFT_EYE    14 
#define SERVO_LEFT_LIT    15 
#define SERVO_LEFT_BROW   13

#define SERVO_RIGHT_EYE    0 // done
#define SERVO_RIGHT_LIT    1 // done
#define SERVO_RIGHT_BROW   3

#define LED_LEFT_RED       4
#define LED_LEFT_GREEN     5
#define LED_LEFT_BLUE      6

#define LED_RIGHT_RED      7
#define LED_RIGHT_GREEN    8
#define LED_RIGHT_BLUE     9

#define SERVO_FREQ        50 // Analog servos run at ~50 Hz updates

#define SERVOMIN          150 // This is the 'minimum' pulse length count (out of 4096)
#define SERVOMAX          600 // This is the 'maximum' pulse length count (out of 4096)

#define NODDING_MIN      150
#define NODDING_MAX      500
#define NODDING_CENTER   (NODDING_MIN + ((NODDING_MAX - NODDING_MIN) >> 1))

#define LEFT_EYE_MIN      150
#define LEFT_EYE_MAX      475
#define LEFT_EYE_CENTER   (LEFT_EYE_MIN + ((LEFT_EYE_MAX - LEFT_EYE_MIN) >> 1))

#define LEFT_LIT_MIN      150
#define LEFT_LIT_MAX      600
#define LEFT_LIT_CENTER   (LEFT_LIT_MIN + ((LEFT_LIT_MAX - LEFT_LIT_MIN) >> 1))

#define LEFT_BROW_MIN     150
#define LEFT_BROW_MAX     600
#define LEFT_BROW_CENTER  (LEFT_BROW_MIN + ((LEFT_BROW_MAX - LEFT_BROW_MIN) >> 1))

#define RIGHT_EYE_MIN  150
#define RIGHT_EYE_MAX  430
#define RIGHT_EYE_CENTER  (RIGHT_EYE_MIN + ((RIGHT_EYE_MAX - RIGHT_EYE_MIN) >> 1))

#define RIGHT_LIT_MIN  150
#define RIGHT_LIT_MAX  600
#define RIGHT_LIT_CENTER  (RIGHT_LIT_MIN + ((RIGHT_LIT_MAX - RIGHT_LIT_MIN) >> 1))

#define RIGHT_BROW_MIN 150
#define RIGHT_BROW_MAX 600
#define RIGHT_BROW_CENTER (RIGHT_BROW_MIN + ((RIGHT_BROW_MAX - RIGHT_BROW_MIN) >> 1))

void face_init(void);
void messageCb( const face::Servo_Array& msg);

#endif
